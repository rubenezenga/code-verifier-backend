import { Get, Query, Route, Tags } from 'tsoa'
import type { BasicResponse } from './types'
import type { IHelloController } from './interfaces'
import { LogSuccess } from '../utils/logger'

@Route('/api/hello')
@Tags('HelloController')
export class HelloController implements IHelloController {
    /**
     * Endpoint to retreive a Message "Hello {name}" in JSON
     * @param {string | undefined} name Name of user to be greeted
     * @returns { BasicResponse } Promise of Basicresponse
     */
    @Get('/')
    public async getMessage (@Query()name: string | undefined): Promise<BasicResponse> {
        LogSuccess('[api/hello] Get Request')
        // Handle nullish or empty name
        const safeName = name ?? 'World' // Usa "World" como valor por defecto si `name` es null o undefined
        return {
            message: `Hello, ${safeName}!`
        }
    }
}
