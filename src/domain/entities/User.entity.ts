import mongoose from 'mongoose'

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export const userEntity = () => {
    const userSchema = new mongoose.Schema({
        name: String,
        email: String,
        age: Number
    })

    return mongoose.model('Users', userSchema)
}
